import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import NavigationStack from './app/screens'

export default class App extends React.Component {
  render() {
    return (
      <NavigationStack />
    );
  }
}
