/*
  Fake response object for Instagram API endpoint:
  GET: /users/self/media/liked

  Some properties were removed and only kept the one
  meaningful for this prototype
*/
const getInstagramLikes = {
    data: [
      {
        caption: "Pretty big day at the court! #basketball #airmaxjordan #mj #legend",
        link: "",
        created_time: "",
        images: {
            low_resolution: {
            },
            thumbnail: {
              url: "https://images.pexels.com/photos/305244/pexels-photo-305244.jpeg?h=350&auto=compress&cs=tinysrgb",
              width: 262,
              height: 175
            },
            standard_resolution: {
              url: "https://images.pexels.com/photos/305244/pexels-photo-305244.jpeg?h=350&auto=compress&cs=tinysrgb",
              width: 524,
              height: 350
            }
        },
        type: "image",
        tags: ['basketball', 'airmaxjordan', 'mj', 'legend'],
        id: "0001",
        user: {
            username: "kevin",
            full_name: "Kevin S",
            profile_picture: "https://images.pexels.com/photos/159831/basketball-basketball-player-high-school-basketball-game-159831.jpeg?h=350&auto=compress&cs=tinysrgb",
            id: "1"
        }
    },
    {
      caption: "Me and my bike #bicycle #adidas #urban #fashion #man",
      link: "",
      created_time: "",
      images: {
          low_resolution: {
          },
          thumbnail: {
            url: "https://images.pexels.com/photos/281967/pexels-photo-281967.jpeg?h=350&auto=compress&cs=tinysrgb",
            width: 262,
            height: 175
          },
          standard_resolution: {
            url: "https://images.pexels.com/photos/281967/pexels-photo-281967.jpeg?h=350&auto=compress&cs=tinysrgb",
            width: 525,
            height: 350
          }
      },
      type: "image",
      tags: ['bicycle','adidas', 'urban', 'fashion', 'man'],
      id: "0002",
      user: {
          username: "mrmatt",
          full_name: "Matthew Moore",
          profile_picture: "https://images.pexels.com/photos/375880/pexels-photo-375880.jpeg?h=350&auto=compress&cs=tinysrgb",
          id: "2"
      }
    },
    {
      caption: "Cruising #fahrrad #fila #bicycle #travelling",
      link: "",
      created_time: "",
      images: {
          low_resolution: {
          },
          thumbnail: {
            url: "https://images.pexels.com/photos/8401/night-sport-bike-bicycle.jpg?h=350&auto=compress&cs=tinysrgb",
            width: 130,
            height: 175
          },
          standard_resolution: {
            url: "https://images.pexels.com/photos/8401/night-sport-bike-bicycle.jpg?h=350&auto=compress&cs=tinysrgb",
            width: 259,
            height: 350
          }
      },
      type: "image",
      tags: ['fahrrad','fila', 'bicycle', 'travelling'],
      id: "0003",
      user: {
          username: "bikforever",
          full_name: "Douglas Smith",
          profile_picture: "https://images.pexels.com/photos/93776/pexels-photo-93776.jpeg?h=350&auto=compress&cs=tinysrgb",
          id: "3"
      }
    }
  ]
};

export default getInstagramLikes;
