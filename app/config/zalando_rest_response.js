/*
  Fake response object from a GET query on Zalando backend
  that returns a list of suggested objects given a list of tags.
*/
const getZalandoItems = {
  data: [
    {
        instagram_id: "0001",
        id: "0",
        name: "IMPACT TR",
        brand: "Jordan",
        price: "99.50 €",
        description: "IMPACT TR - Trainings- / Fitnessschuh - black/gym red/wolf grey",
        picture: {
          thumbnail: "https://mosaic01.ztat.net/vgs/media/pdp-gallery/JO/C4/2A/00/8Q/11/JOC42A008-Q11@12.jpg",
          original: "https://mosaic01.ztat.net/vgs/media/pdp-gallery/JO/C4/2A/00/8Q/11/JOC42A008-Q11@12.jpg"
        }
    },
    {
        instagram_id: "0001",
        id: "1",
        name: "ELITE COMPETITION",
        brand: "Nike Performance",
        price: "34.95 €",
        description: "ELITE COMPETITION - Basketball - amber/black/platinum",
        picture: {
          thumbnail: "https://mosaic01.ztat.net/vgs/media/pdp-gallery/N1/24/4E/06/NH/11/N1244E06N-H11@6.jpg",
          original: "https://mosaic01.ztat.net/vgs/media/pdp-gallery/N1/24/4E/06/NH/11/N1244E06N-H11@6.jpg"
        }
    },
    {
        instagram_id: "0001",
        id: "2",
        name: "ALPHA DRY",
        brand: "Jordan",
        price: "34.95 €",
        description: "ALPHA DRY - T-Shirt print - white/black",
        picture: {
          thumbnail: "https://mosaic01.ztat.net/vgs/media/pdp-gallery/JO/C4/2D/00/0A/11/JOC42D000-A11@14.1.jpg",
          original: "https://mosaic01.ztat.net/vgs/media/pdp-gallery/JO/C4/2D/00/0A/11/JOC42D000-A11@14.1.jpg"
        }
    },
    {
        instagram_id: "0002",
        id: "3",
        name: "SHIELD COACHES",
        brand: "Nike SB",
        price: "69.95 €",
        description: "SHIELD COACHES - Leichte Jacke - dark team red/obsidian",
        picture: {
          thumbnail: "https://mosaic01.ztat.net/vgs/media/pdp-gallery/NS/42/2H/00/7G/11/NS422H007-G11@10.1.jpg",
          original: "https://mosaic01.ztat.net/vgs/media/pdp-gallery/NS/42/2H/00/7G/11/NS422H007-G11@10.1.jpg"
        }
    },
    {
        instagram_id: "0002",
        id: "4",
        name: "SUPER SLIM LIGHT",
        brand: "Abercrombie & Fitch",
        price: "89.95 €",
        description: "SUPER SLIM LIGHT - Jeans Slim Fit - light blue",
        picture: {
          thumbnail: "https://mosaic02.ztat.net/vgs/media/pdp-gallery/A0/F2/2G/00/WK/11/A0F22G00W-K11@12.jpg",
          original: "https://mosaic02.ztat.net/vgs/media/pdp-gallery/A0/F2/2G/00/WK/11/A0F22G00W-K11@12.jpg"
        }
    },
    {
        instagram_id: "0002",
        id: "5",
        name: "IVYLAND",
        brand: "Dickies",
        price: "54.95 €",
        description: "IVYLAND - Hemd - gray",
        picture: {
          thumbnail: "https://mosaic02.ztat.net/vgs/media/pdp-gallery/DI/62/2D/02/CC/11/DI622D02C-C11@12.jpg",
          original: "https://mosaic02.ztat.net/vgs/media/pdp-gallery/DI/62/2D/02/CC/11/DI622D02C-C11@12.jpg"
        }
    },
    {
        instagram_id: "0002",
        id: "5",
        name: "LA TRAINER OG",
        brand: "adidas Originals",
        price: "99.95 €",
        description: "LA TRAINER OG - Sneaker low - orange/noble teal/footwear white",
        picture: {
          thumbnail: "https://mosaic01.ztat.net/vgs/media/pdp-gallery/AD/11/5B/07/BH/11/AD115B07B-H11@12.jpg",
          original: "https://mosaic01.ztat.net/vgs/media/pdp-gallery/AD/11/5B/07/BH/11/AD115B07B-H11@12.jpg"
        }
    },
    {
        instagram_id: "0003",
        id: "6",
        name: "Sweatshirt",
        brand: "Fila",
        price: "64.95 €",
        description: "Sweatshirt - bright white",
        picture: {
          thumbnail: "https://mosaic02.ztat.net/vgs/media/pdp-gallery/1F/I2/2S/00/2A/11/1FI22S002-A11@12.jpg",
          original: "https://mosaic02.ztat.net/vgs/media/pdp-gallery/1F/I2/2S/00/2A/11/1FI22S002-A11@12.jpg"
        }
    },
    {
        instagram_id: "0003",
        id: "7",
        name: "T-Shirt basic",
        brand: "Fila",
        price: "23.95 €",
        description: "T-Shirt basic - black iris",
        picture: {
          thumbnail: "https://mosaic01.ztat.net/vgs/media/pdp-gallery/1F/I2/2O/00/0Q/11/1FI22O000-Q11@10.jpg",
          original: "https://mosaic01.ztat.net/vgs/media/pdp-gallery/1F/I2/2O/00/0Q/11/1FI22O000-Q11@10.jpg"
        }
    },
    {
        instagram_id: "0003",
        id: "8",
        name: "SANTANA",
        brand: "Fila",
        price: "47.95 €",
        description: "SANTANA - Shorts - peacoat blue",
        picture: {
          thumbnail: "https://mosaic01.ztat.net/vgs/media/pdp-gallery/1F/I4/2E/00/1K/11/1FI42E001-K11@7.jpg",
          original: "https://mosaic01.ztat.net/vgs/media/pdp-gallery/1F/I4/2E/00/1K/11/1FI42E001-K11@7.jpg"
        }
    },
    {
        instagram_id: "0003",
        id: "9",
        name: "JEAN",
        brand: "Fila",
        price: "90.95 €",
        description: "JEAN - Trainingsanzug - peacoat blue/white/red",
        picture: {
          thumbnail: "https://mosaic02.ztat.net/vgs/media/pdp-gallery/1F/I4/2K/00/1K/11/1FI42K001-K11@20.jpg",
          original: "https://mosaic02.ztat.net/vgs/media/pdp-gallery/1F/I4/2K/00/1K/11/1FI42K001-K11@20.jpg"
        }
    }
  ]
}

export default getZalandoItems;
