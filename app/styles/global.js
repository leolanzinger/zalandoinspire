const styles = {
  black: '#000',
  white: '#fff',
  grey: '#A8A8A8',
  orange: '#FF6900',
  darkWhite: '#fafafa',
  titleSize: '25',
  headerHeight: 40,
  headerMargin: 10,
  defaultSpacing: 20,
  smallSpacing: 5
}
export default styles;
