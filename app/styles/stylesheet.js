import styles from './global.js';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  header: {
    backgroundColor: styles.white
  },
  container: {
    backgroundColor: styles.darkWhite,
    height: '100%'
  },
  headerRightIconContainer: {
    flex: 1,
    flexDirection: 'row',
    height: styles.headerHeight,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: styles.headerMargin
  },
  headerLeftLogoContainer: {
    flex: 1,
    flexDirection: 'row',
    height: styles.headerHeight,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: styles.headerMargin
  },
  singleItemScreenContainer: {
    backgroundColor: styles.white,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'top',
    alignItems: 'center'
  }
});
