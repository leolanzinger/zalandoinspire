import { StackNavigator } from 'react-navigation';
import React from 'react';
import HomeScreen from './HomeScreen';
import ItemScreen from './ItemScreen';
import InstagramScreen from './InstagramScreen';
import { StyleSheet, Text, View, Button } from 'react-native';


const NavigationStack = StackNavigator({
  Home: {
    screen: HomeScreen
  },
  Item: {
    screen: ItemScreen
  },
  Instagram: {
    screen: InstagramScreen
  }
});

export default NavigationStack
