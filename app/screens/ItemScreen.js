import React, {Component} from 'react';
import { StackNavigator } from 'react-navigation';
import {ScrollView, View, Button, Tile, Image, Divider, TouchableOpacity} from '@shoutem/ui';
import { StyleSheet, Text, TouchableWithoutFeedback } from 'react-native';
import ActionButton from 'react-native-action-button';
import styles from '../styles/global';
import stylesheet from '../styles/stylesheet';
import getInstagramLikes from '../config/instagram_rest_response';
import Icon from 'react-native-vector-icons/FontAwesome';
import InstagramCard from '../components/InstagramCard';
import ItemScreenHeader from '../components/ItemScreenHeader';

class ItemScreen extends Component {
  static navigationOptions = ({navigation}) => {
    return {
      title: navigation.state.params.title,
      headerStyle: stylesheet.header
    };
  };
  constructor(props) {
    super(props);
    this.mapZalandoToInstagram = this.mapZalandoToInstagram.bind(this);
  }
  mapZalandoToInstagram(item, instagramList) {
    const instagramItem = instagramList.find(i => i.id === item.instagram_id);
    return instagramItem;
  }
  render() {
    const { params } = this.props.navigation.state;
    const instagramObject = this.mapZalandoToInstagram(params.object, getInstagramLikes.data);
    return (
      <ScrollView style={{backgroundColor: styles.white, flex: 1}}>
        <ItemScreenHeader img={params.object.picture.original} />
        <View style={{
          paddingLeft: styles.defaultSpacing,
          paddingTop: styles.defaultSpacing,
          paddingRight: styles.defaultSpacing
        }}>
          <Text style={{color: styles.black, size: styles.titleSize, fontWeight: 'bold'}}>
            {params.object.brand} - {params.object.name}
          </Text>
          <Text style={{color: styles.grey, size: styles.titleSize, fontWeight: 'bold'}}>
            {params.object.price}
          </Text>
        </View>
        <View style={{padding: styles.defaultSpacing}}>
          <Text style={{marginBottom: styles.defaultSpacing}}>
            {params.object.description}
          </Text>
          <Divider styleName="line" />
        </View>
        <View style={{padding: styles.defaultSpacing}}>
          <Text style={{marginBottom: 10}}>
            <Icon name="instagram" size={18} marginRight={15} color={styles.black} /> Because you like:
          </Text>
          <TouchableOpacity
            onPress={
              () => this.props.navigation.navigate(
                'Instagram',
                {
                  object: instagramObject,
                  title: instagramObject.user.username
                }
              )
            }>
            <InstagramCard instagram={instagramObject}/>
          </TouchableOpacity>
        </View>
        <Button
          style={{
            width: 60,
            height: 60,
            borderRadius: 30,
            backgroundColor: styles.orange,
            position: 'absolute',
            right: 20,
            top: 250,
            flex: 1,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
            padding: 0
          }}>
          <Icon name="shopping-bag" size={20} color={styles.white} />
        </Button>
      </ScrollView>
    );
  }
}

export default ItemScreen;
