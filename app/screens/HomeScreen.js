import React, {Component} from 'react';
import { StackNavigator } from 'react-navigation';
import { StyleSheet, Text, View, Button, TouchableWithoutFeedback, Image } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import GridFeed from '../components/GridFeed';
import ListFeed from '../components/ListFeed';
import styles from '../styles/global';
import stylesheet from '../styles/stylesheet';
import zalandoLogo from '../../assets/icons/cropped-zalando.png';
import getZalandoItems from '../config/zalando_rest_response';
import getInstagramLikes from '../config/instagram_rest_response';

class GridViewScreen extends Component {
  static navigationOptions = ({navigation}) => {
    const { params = {} } = navigation.state;
    return {
      headerStyle: stylesheet.header,
      headerLeft:
        <View style={stylesheet.headerLeftLogoContainer}>
          <Image
            style={{width: 22, height: 22, marginRight: 5}}
            source={zalandoLogo}
            />
          <Text>INSPIRE</Text>
        </View>,
      headerRight:
        <View style={stylesheet.headerRightIconContainer}>
          <TouchableWithoutFeedback onPress={() => {params.changeView(0)}}>
            <Icon name="view-list" size={22} marginRight={25} color={styles.black} />
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={() => {params.changeView(1)}}>
            <Icon name="view-grid" size={18} color={styles.black} />
          </TouchableWithoutFeedback>
        </View>
      };
  };
  constructor(props) {
    super(props);
    this.state = {list: true};
  }
  componentDidMount() {
    this.props.navigation.setParams({changeView: this.changeViewState});
  }
  changeViewState = (val) => {
    (val == 1) ? this.setState({list: false}) : this.setState({list: true});
  }
  render() {
    var view = this.state.list ?
               <ListFeed navigation={this.props.navigation}
                         zalandoItems={getZalandoItems}
                         instagramItems={getInstagramLikes}/> :
               <GridFeed navigation={this.props.navigation}
                         zalandoItems={getZalandoItems} />;
    return (
      <View style={stylesheet.container}>
        {view}
      </View>
    );
  }
}

export default GridViewScreen;
