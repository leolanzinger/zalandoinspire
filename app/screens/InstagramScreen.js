import React, {Component} from 'react';
import { StackNavigator } from 'react-navigation';
import {View, Image, Tile, Divider, Caption} from '@shoutem/ui';
import Icon from 'react-native-vector-icons/FontAwesome';
import styles from '../styles/global';
import stylesheet from '../styles/stylesheet';
import getZalandoItems from '../config/zalando_rest_response';
import HorizontalItemList from '../components/HorizontalItemList';

class InstagramScreen extends Component {
  static navigationOptions = ({navigation}) => {
    return {
      headerStyle: stylesheet.header,
      title: <Icon name="instagram" size={18} marginRight={15} color={styles.black} />
    };
  };
  constructor(props) {
    super(props);
    this.mapInstagramToZalando = this.mapInstagramToZalando.bind(this);
  }
  mapInstagramToZalando(item, zalandoList) {
    var zalandoItems = zalandoList.filter(i => i.instagram_id === item.id);
    return zalandoItems;
  }
  render() {
    const { params } = this.props.navigation.state;
    const zalandoItems = this.mapInstagramToZalando(params.object, getZalandoItems.data);
    return (
      <View style={{
        backgroundColor: styles.white,
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'top',
        alignItems: 'center'
      }}>
        <Image
          source={{uri: params.object.images.standard_resolution.url}}
          styleName="large-banner" />
        <View style={{padding: styles.defaultSpacing}}>
          <View style={{flexDirection: 'row', marginBottom: styles.smallSpacing}}>
            <Image source={{uri: params.object.user.profile_picture}} style={{marginRight: styles.smallSpacing}} styleName="small-avatar"/>
            <Caption style={{fontWeight: 'bold'}}>{params.object.user.username}</Caption>
          </View>
          <Caption style={{color: styles.black}}>
            {params.object.caption}
          </Caption>
        </View>
        <Divider styleName="line" style={{marginBottom: styles.defaultSpacing}}/>
        <HorizontalItemList items={zalandoItems} navigation={this.props.navigation}/>
      </View>
    );
  }
}

export default InstagramScreen;
