import React, {Component} from 'react';
import { StyleSheet, Text, View, Button, TouchableWithoutFeedback } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import styles from '../styles/global';
import stylesheet from '../styles/stylesheet';
import { GridRow, Image, ListView, TouchableOpacity } from '@shoutem/ui';

class GridFeed extends Component {
  constructor(props) {
    super(props);
    this.renderRow = this.renderRow.bind(this);
  }
  renderRow(rowData, sectionId, index) {
      const cellViews = rowData.map((item, id) => (
        <TouchableOpacity key={id}
          style={{
            height: 100,
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center'
          }}
          onPress={() => this.props.navigation.navigate('Item', {object: item, title: item.brand + '- ' + item.name})}
          >
          <Image source={{uri: item.picture.thumbnail}} styleName="small" />
        </TouchableOpacity>
      ));

      return (
          <GridRow columns={3} style={{backgroundColor: styles.white}}>
              {cellViews}
          </GridRow>
      )
  }
  render() {
    const groupedItems = GridRow.groupByRows(this.props.zalandoItems.data, 3);
    return (
      <ListView
        data={groupedItems}
        renderRow={this.renderRow} />
    );
  }
}

export default GridFeed;
