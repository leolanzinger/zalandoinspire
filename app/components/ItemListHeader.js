import React, {Component} from 'react';
import {View, Text, Image, Tile, TouchableOpacity} from '@shoutem/ui';
import styles from '../styles/global';
import stylesheet from '../styles/stylesheet';

class ItemListHeader extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <TouchableOpacity
        onPress={
          () => this.props.navigation.navigate(
            'Item',
            {
              object: this.props.item,
              title: this.props.item.brand + '- ' + this.props.item.name
            }
          )
        }>
        <View style={{
          paddingLeft: styles.defaultSpacing,
          paddingTop: styles.defaultSpacing,
          paddingRight: styles.defaultSpacing
        }}>
          <Text style={{color: styles.black, size: styles.titleSize, fontWeight: 'bold'}}>
            {this.props.item.brand} - {this.props.item.name}
          </Text>
          <Text style={{color: styles.grey, size: styles.titleSize, fontWeight: 'bold'}}>
            {this.props.item.price}
          </Text>
        </View>
        <Tile style={{
          backgroundColor: styles.white,
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center'
        }}>
          <Image
            source={{uri: this.props.item.picture.original}}
            style={{
              flex: 1,
              width: 300,
              height: 300,
              resizeMode: 'cover'
            }}
            styleName="large-wide" />
        </Tile>
      </TouchableOpacity>
    );
  }
}

export default ItemListHeader;
