import React, {Component} from 'react';
import {View, Text, Image, Tile, Caption} from '@shoutem/ui';
import styles from '../styles/global';

class InstagramCard extends Component {
  render() {
    return (
      <View>
        <View style={{
          shadowColor: '#000',
          shadowOpacity: 0.16,
          shadowRadius: 2,
          shadowOffset: {height: 2, width: 0},
          flex: 1,
          flexDirection: 'row'
        }}>
          <Image source={{uri: this.props.instagram.images.thumbnail.url}}
                 style={{
                   width: 100,
                   height: 100
                 }}/>
          <View style={{flex: 1, flexDirection: 'column', padding: 10}}>
            <View style={{flex: 1 , flexDirection: 'row', marginBottom: 5}}>
              <Image source={{uri: this.props.instagram.user.profile_picture}} style={{marginRight: 5}} styleName="small-avatar"/>
              <Caption style={{fontWeight: 'bold'}}>{this.props.instagram.user.username}</Caption>
            </View>
            <Caption style={{color: styles.black}}>{this.props.instagram.caption}</Caption>
          </View>
        </View>
      </View>
    );
  }
}

export default InstagramCard;
