import React, {Component} from 'react';
import {View, Text, Image, Tile, TouchableOpacity} from '@shoutem/ui';
import styles from '../styles/global';
import stylesheet from '../styles/stylesheet';

class ItemScreenHeader extends Component {
  render() {
    return (
      <Tile style={{
        backgroundColor: styles.white,
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
      }}>
        <Image
          source={{uri: this.props.img}}
          style={{
            flex: 1,
            width: 300,
            height: 300,
            resizeMode: 'cover'
          }}
          styleName="large-wide" />
      </Tile>
    );
  }
}

export default ItemScreenHeader;
