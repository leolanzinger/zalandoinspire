import React, {Component} from 'react';
import { StackNavigator } from 'react-navigation';
import {View, Image, TouchableOpacity, Divider, Caption, ListView, Card, Subtitle} from '@shoutem/ui';
import {Text} from 'react-native';
import styles from '../styles/global';
import stylesheet from '../styles/stylesheet';

class HorizontalItemList extends Component {
  constructor(props) {
    super(props);
    this.renderRow = this.renderRow.bind(this);
  }
  renderRow(item) {
    return (
      <TouchableOpacity
        onPress={
        () => this.props.navigation.navigate(
          'Item',
          {
            object: item,
            title: item.brand + '- ' + item.name
          }
        )}
        >
        <Card style={{
          backgroundColor: styles.white,
          shadowColor: styles.black,
          shadowOpacity: 0.16,
          shadowRadius: 2,
          shadowOffset: {height: 2, width: 0},
          marginRight: 10,
          flex: 1
        }}>
          <View style={{
            flex: 1,
            flexDirection: 'row',
            alignItems: 'center',
            justifyConten: 'center'
          }}>
            <Image
              styleName="medium-square"
              source={{uri: item.picture.thumbnail}}
            />
          </View>
          <View styleName="content">
            <Subtitle>{item.name}</Subtitle>
            <Caption>{item.price}</Caption>
          </View>
        </Card>
      </TouchableOpacity>
    );
  }
  render() {
    return (
      <ListView
        style={{
                list: {backgroundColor: styles.white},
                listContent: {backgroundColor: styles.white}
              }}
        horizontal={true}
        data={this.props.items}
        renderRow={this.renderRow} />
    );
  }
}

export default HorizontalItemList;
