import React, {Component} from 'react';
import { StyleSheet, Text, Button, TouchableWithoutFeedback } from 'react-native';
import {ListView, View, TouchableOpacity} from '@shoutem/ui';
import Icon from 'react-native-vector-icons/FontAwesome';
import styles from '../styles/global';
import InstagramCard from '../components/InstagramCard';
import ItemListHeader from '../components/ItemListHeader';

class ListFeed extends Component {
  constructor(props) {
    super(props);
    this.renderRow = this.renderRow.bind(this);
    this.mapZalandoToInstagram = this.mapZalandoToInstagram.bind(this);
  }
  mapZalandoToInstagram(item, instagramList) {
    const instagramItem = instagramList.find(i => i.id === item.instagram_id);
    return instagramItem;
  }
  renderRow(item) {
    const instagramObject = this.mapZalandoToInstagram(item, this.props.instagramItems.data);
    return (
      <View styleName="v-start" style={{backgroundColor: styles.white, marginBottom: styles.defaultSpacing}}>
        <ItemListHeader item={item} navigation={this.props.navigation} />
        <View style={{padding: styles.defaultSpacing}}>
          <Text style={{marginBottom: 10}}>
            <Icon name="instagram" size={18} marginRight={15} color={styles.black} /> Because you like:
          </Text>
          <TouchableOpacity
            onPress={
              () => this.props.navigation.navigate(
                'Instagram',
                {
                  object: instagramObject,
                  title: instagramObject.user.username
                }
              )
            }>
            <InstagramCard instagram={instagramObject}/>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
  render() {
    return (
      <ListView
        style={styles.darkWhite}
        data={this.props.zalandoItems.data}
        renderRow={this.renderRow} />
    );
  }
}

export default ListFeed;
